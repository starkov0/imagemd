//
//  Line.swift
//  imageMD
//
//  Created by Pierre Starkov on 30/06/15.
//  Copyright (c) 2015 Pierre Starkov. All rights reserved.
//

import Foundation
import UIKit

class PointsSet: NSObject {
    var _points = [CGPoint]()
    
    func appendPoint(point: CGPoint) {
        _points.append(point)
    }
    
    func getPoints() -> [CGPoint] {
        return _points
    }
    
}