//
//  DrawView.swift
//  imageMD
//
//  Created by Pierre Starkov on 30/06/15.
//  Copyright (c) 2015 Pierre Starkov. All rights reserved.
//

// TODO : 
// - déplacement des Labels
// - screenshot

import Foundation
import UIKit

class DrawView: UIView, UITextFieldDelegate {
    // points
    var _pointsSetArray = [PointsSet]()
    var _currentPointsSet: PointsSet!

    // textField
    var _textViewArray = [UITextField]()
    var _currentTextView: UITextField!
    var _currentTextViewY: CGFloat!
    var _textViewHeight: CGFloat = 40
    var _isWritng: Bool = false
    
    // keyboard
    var _keyboardHeight: CGFloat!
    var _keyboardIsOn: Bool = false
    
    // mainView height
    var _mainViewHeight: CGFloat!
    
    // INIT
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.myInit()
    }
    
    required override init(frame: CGRect) {
        super.init(frame: frame)
        self.myInit()
    }
    
    func myInit() {
        // opaque
        self.opaque = false
        
        // tap
        var tapGesture = UITapGestureRecognizer(target: self, action: Selector("handleTap:"))
        tapGesture.numberOfTapsRequired = 2
        self.addGestureRecognizer(tapGesture)
        
        // keyboard
        var center: NSNotificationCenter = NSNotificationCenter.defaultCenter()
        center.addObserver(self, selector: "keyboardWillShow:", name: UIKeyboardWillShowNotification, object: nil)
        center.addObserver(self, selector: "keyboardWillHide:", name: UIKeyboardWillHideNotification, object: nil)
    }
    
    func handleTap(sender : UITapGestureRecognizer) {
        // create
        var _textView = UITextField(
            frame: CGRectMake(
                0,
                sender.locationInView(self).y,
                self.bounds.width,
                _textViewHeight
            ))
        _textView.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
        _textView.textColor = UIColor.whiteColor()
        _textView.textAlignment = NSTextAlignment.Center
        _textView.delegate = self
        
        // append to array
        _textViewArray.append(_textView)
        
        // add to subview
        self.addSubview(_textView)
        
        // keyboard open
        _textView.becomeFirstResponder()
    }
    
    func getCurrentTextView() -> UITextField {
        println("_textViewArray: \(_textViewArray.count)")
        
        var textView: UITextField!
        for var i=0; i<_textViewArray.count; i++ {
            var _textView = _textViewArray[i] as UITextField
            if _textView.isFirstResponder() {
                textView = _textView
            }
        }
        return textView
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        println("textFieldDidBeginEditing")
        
        // main view
        _mainViewHeight = self.superview!.superview!.bounds.height

        // currentTextView
        _currentTextView = self.getCurrentTextView()
        _currentTextViewY = _currentTextView.frame.origin.y
        println(_currentTextView)
        
        // animation
        if _keyboardIsOn {
            UITextField.animateWithDuration(0.15, delay: 0.15, options: UIViewAnimationOptions.CurveEaseInOut, animations: {
                self._currentTextView.frame = CGRectMake(
                    0,
                    self._mainViewHeight - self._keyboardHeight - self._textViewHeight,
                    self.bounds.width,
                    self._textViewHeight
                )
            }, completion: nil)
        }
        
    }
    
    func keyboardWillShow(notification: NSNotification) {
        println("keyboardWillShow")
        
        // keyboard height
        var info:NSDictionary = notification.userInfo!
        var keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as! NSValue).CGRectValue()
        _keyboardHeight = keyboardSize.height
        
        // currentTextView
        _currentTextView = self.getCurrentTextView()
        _currentTextViewY = _currentTextView.frame.origin.y
        println(_currentTextView)

        if !_keyboardIsOn {
            // animation
            UITextField.animateWithDuration(0.15, delay: 0.15, options: UIViewAnimationOptions.CurveEaseInOut, animations: {
                self._currentTextView.frame = CGRectMake(
                    0,
                    self._mainViewHeight - self._keyboardHeight - self._textViewHeight,
                    self.bounds.width,
                    self._textViewHeight
                )
            }, completion: nil)
        }
        
        // bool
        _isWritng = true
        _keyboardIsOn = true
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        // animation
        UIView.animateWithDuration(0.25, delay: 0.25, options: UIViewAnimationOptions.CurveEaseInOut, animations: {
            self._currentTextView.frame = CGRectMake(
                0,
                self._currentTextViewY,
                self.bounds.width,
                self._textViewHeight
            )
        }, completion: nil)
    }
    
    func keyboardWillHide(notification: NSNotification) {
        // bool
        _isWritng = false
        _keyboardIsOn = false
    }

    // REMOVE
    func removeLastPointSet() {
        if _pointsSetArray.count > 0 {
            _pointsSetArray.removeLast()
            self.setNeedsDisplay()
        }
    }
    
    func removeLastTextView() {
        if _textViewArray.count > 0 {
            var textField = _textViewArray.last
            textField?.removeFromSuperview()
            _textViewArray.removeLast()
        }
    }
    
    // DRAW
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        if _isWritng {
            _isWritng = false
            self.endEditing(true)
        } else {
            var touch = touches.first as! UITouch
            _currentPointsSet = PointsSet()
            _currentPointsSet.appendPoint(touch.locationInView(self))
            _pointsSetArray.append(_currentPointsSet)
        }
    }
    
    override func touchesMoved(touches: Set<NSObject>, withEvent event: UIEvent) {
        if !_isWritng {
            var touch = touches.first as! UITouch
            _currentPointsSet.appendPoint(touch.locationInView(self))
            self.setNeedsDisplay()
        }
    }
    
    override func drawRect(rect: CGRect) {
        for _pointsSet in _pointsSetArray {
            var path = UIBezierPath()
            path.lineWidth = 5.0
            path.moveToPoint(_pointsSet.getPoints()[0])
            path.interpolatePointsWithHermite(_pointsSet.getPoints())
            path.lineJoinStyle = kCGLineJoinRound
            path.lineCapStyle = kCGLineCapRound
            UIColor.blueColor().setStroke()
            path.stroke()
        }
    }

}